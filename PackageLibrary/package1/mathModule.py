#mathModule.py

import math

def get_cos(x=1):
	return math.cos(x)


def get_sin(x=1):
	return math.sin(x)


def get_tan(x=1):
	return math.tan(x)


def get_sqrt(x=1):
	return math.sqrt(x)


def get_par(x=1):
	return x % 2