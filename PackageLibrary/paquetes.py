#paquetes.py
import random

from package1.mathModule import get_cos, get_par
from package1.dateModule import get_datetime


q = random.randint(1, 10)
print("El coseno de {0} es: {1}". format(q, get_cos(q)))


print("La fecha y Hora actual es: {0}".format(get_datetime()))

if get_par(q) == 0:
	response = True
else:
	response = False

print("El numero {0} es Par? --- {1}".format(q, response))