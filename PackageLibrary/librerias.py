#librerias.py


# forma de importar como packetes
#from random import randint
# forma de importar como libreria

import random
import math



# print(randint(1, 100))
# print(random.randint(1, 100))
# x = random.random()
# print("Coseno de {0} es: {1}".format(x, math.cos(x)))
# y = random.randint(1, 100)
# print("La raiz cuadrada de {0} es: {1}".format(y, math.sqrt(y)))


def get_cos(x=1):
	return math.cos(x)


r = random.randint(1, 10)
print("El coseno de {0}, es: {1}".format(r, get_cos(r)))