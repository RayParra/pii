#coding: utf-8
#main.py


from kivy.app import App
from kivy.core import window
from kivy.uix.label import Label
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button

import random


def click():
	print(random.randint(1, 100))
	return str(random.randint(1, 100))


def build():
	layout = FloatLayout()
	
	label = Label(text=click())
	label.size_hint = None, None
	label.x = 20
	label.y = 50

	txt = TextInput()
	txt.size_hint = None, None
	txt.height = 300
	txt.width = 400
	txt.x = 60
	txt.y = 250

	btn = Button(text="ClickMe")
	btn.size_hint = None, None
	btn.height = 50
	btn.width = 200
	btn.x = 200
	btn.y = 100

	btn.on_press = click


	layout.add_widget(label)
	layout.add_widget(txt)
	layout.add_widget(btn)

	return layout



app = App()

window = window.Window
window.size = 600, 600

app.build = build

app.run()