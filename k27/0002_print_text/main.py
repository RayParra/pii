#coding: utf-8
#main.py

# Library
from kivy.app import App
from kivy.uix.label import Label


#Components Functions
def build():
	return Label(text="Curso Python Kivy", italic=True, font_size=50)


#Instances
app = App()
app.build = build
app.run()