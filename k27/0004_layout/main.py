#coding:utf-8
#main.py

from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput


import random


class main_window(App):
	def build(self):
		layout = FloatLayout()

		self.lbl = Label(
				text = "Kivy Lang",
				size_hint = (None, None),
				x = 100,
				y = 200
			)
		

		self.txt = TextInput(
				size_hint = (None, None),
				width = 300,
				x = 200,
				y = 300

			)
		
		self.btn = Button(

				text = "Kivy Lang",
				size_hint = (None, None),
				x = 10,
				y = 20,
				on_press = self.clear_label
			)
		layout.add_widget(self.lbl)
		layout.add_widget(self.btn)
		layout.add_widget(self.txt)
		
		return layout

	def clear_label(self, instance):
		self.lbl.text = str(random.randint(1, 100))

main_window().run()