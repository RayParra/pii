# coding: utf-8
#main.py


from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button



class Custom_btn(Button):
	def __init__(self, **kwargs):
		super().__init__(**kwargs)

		self.font_size = "30sp"

	



class Calculator(GridLayout):


	def evalexp(self, instance):
		self.ti.text = str(eval(self.ti.text))


	def clear(self, instance):
		self.ti.text = ""


	def press(self, button):
		#print("Click")
		self.ti.text += button.text


	def __init__(self, **kwargs):
		super().__init__(**kwargs)

		self.rows = 5
		self.padding = "10dp"
		self.spacing = "10dp"


		self.bl = BoxLayout()
		self.add_widget(self.bl)

		self.ti = TextInput(font_size="30sp", multiline=False, readonly=True)
		self.bl.add_widget(self.ti)

		self.bl = BoxLayout()
		self.add_widget(self.bl)

		self.bl.add_widget(Custom_btn(text="7", on_press=self.press))
		self.bl.add_widget(Custom_btn(text="8", on_press=self.press))
		self.bl.add_widget(Custom_btn(text="9", on_press=self.press))
		self.bl.add_widget(Custom_btn(text="*", on_press=self.press))

		self.bl = BoxLayout()
		self.add_widget(self.bl)

		self.bl.add_widget(Custom_btn(text="4", on_press=self.press))
		self.bl.add_widget(Custom_btn(text="5", on_press=self.press))
		self.bl.add_widget(Custom_btn(text="6", on_press=self.press))
		self.bl.add_widget(Custom_btn(text="-", on_press=self.press))

		self.bl = BoxLayout()
		self.add_widget(self.bl)

		self.bl.add_widget(Custom_btn(text="1", on_press=self.press))
		self.bl.add_widget(Custom_btn(text="2", on_press=self.press))
		self.bl.add_widget(Custom_btn(text="3", on_press=self.press))
		self.bl.add_widget(Custom_btn(text="+", on_press=self.press))


		self.bl = BoxLayout()
		self.add_widget(self.bl)

		self.bl.add_widget(Custom_btn(text="AC", on_press=self.clear))
		self.bl.add_widget(Custom_btn(text="0", on_press=self.press))
		self.bl.add_widget(Custom_btn(text="=", on_press=self.evalexp))
		self.bl.add_widget(Custom_btn(text="/", on_press=self.press))


class Main_Window(App):
	def build(self):
		return Calculator()

Main_Window().run()