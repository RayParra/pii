#classesObjects.py

import random

class Button:
	def __init__(self, x=100, y=100, caption="btn"):
		self.x = x
		self.y = y
		self.caption = caption


	def click(self, clicks):
		if clicks == 1:
			return "Click Principal"
		if clicks == 2:
			return "Click Secundario"

btn = Button(300, 200, "Guardar")

print("El boton esta ubicado en las coordenadas x = {0} - y = {1} -- Tiene el caption: {2}".format(btn.x, btn.y, btn.caption))

r = random.randint(1,2)

print("Click Presionado: {0}".format(btn.click(r)))